// $Id:

// define namespace
Drupal.imageslider = Drupal.imageslider || {};

// the auto attach function runs when everything is ready
Drupal.imageslider.autoAttach = function() {
  
  // make a shortcut to the settings using a local variable
  var settings = Drupal.settings.imageslider;
  
  // find all imageslider containers and attach the JS functionality
  var sliderNum = 0;
  jQuery("div.imageslider").each(function() {
    
		var container = jQuery(this);
		var imageWidth = container.find("li").find("img").width();
		var imageHeight = container.find("li").find("img").height();
		var numImages = container.find("li").size();
		var sliderWidth = imageWidth * numImages;
    
		container.each(function(i) {
      
      // add buttons to control the slider
			jQuery(this).after("<div class='stripTransmitter' id='stripTransmitter" + sliderNum + "'><ul><\/ul><\/div>");
      jQuery(this).find("li").each(function(n) {
				jQuery("div#stripTransmitter" + sliderNum + " ul").append("<li><a title='" + jQuery(this).find("img").attr("alt") + "' href='#'>"+(n+1)+"<\/a><\/li>");												
			});
      
      // attach slider action to each button
			jQuery("div#stripTransmitter" + sliderNum + " a").each(function(z) {
				jQuery(this).bind("click", function(){
				jQuery(this).addClass("current").parent().parent().find("a").not(jQuery(this)).removeClass("current"); // wow!
				var cnt = - (imageWidth*z);
				jQuery(this).parent().parent().parent().prev().find("ul").animate({ left: cnt}, settings.easeTime, settings.easeFunc);
          return false;
				});
			});
      
      // set width of button strip
			jQuery("div#stripTransmitter" + sliderNum).css("width" , imageWidth);
      
      // add an extra class to the current button
			jQuery("div#stripTransmitter" + sliderNum + " a:eq(0)").addClass("current");
		});
		sliderNum++;
  });
}

// Global Killswitch
if (Drupal.jsEnabled) {
  $(document).ready(Drupal.imageslider.autoAttach);
}
