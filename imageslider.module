<?php
// $Id: 

/**
* Implementation of hook_menu
*/
function imageslider_menu($cache) {
  $items = array();
  if ($cache) {
    $items[] = array(
			'title' => t('Image Slider Settings'),
      'description' => t('Configure settings for the sliding image viewer.'),
			'path' => 'admin/settings/imageslider',
			'access' => user_access('administer site configuration'),
			'type' => MENU_NORMAL_ITEM,
			'callback' => 'drupal_get_form',
			'callback arguments' => array('imageslider_admin_settings')
    );
  }
  return $items;
}

/**
 * Implementation of hook_block().
 */
function imageslider_block($op = 'list', $delta = 0) {
  switch ($op) {
    case 'list':
      $blocks[0]['info'] = t('Image Slider');
      return $blocks;
    case 'view':
      $block['subject'] = t('Image Slider');
      $block['content'] = imageslider_block_content();
      return $block;
  }
}

function imageslider_block_content() {
  // add JS and CSS
  $path = drupal_get_path('module', 'imageslider');
  
  // add jQuery.easing if possible
  $easing = false;
  if (file_exists($path . '/jquery.easing.js')) {
    $easing = true;
    drupal_add_js($path . '/jquery.easing.js');
  }
  
  // add imageslider CSS and JS
  drupal_add_css($path . '/imageslider.css');
  drupal_add_js($path . '/imageslider.js');
  
  // add settings JS
  drupal_add_js(array(
    'imageslider' => array(
      'easeFunc' => ($easing) ? variable_get('imageslider_ease_func', '') : '',
      'easeTime' => (int)variable_get('imageslider_ease_time', 750)
    )
  ), 'setting');
  
  // render viewer
  $view = views_get_view('imageslider');
  if (!$view) {
    drupal_set_message("The Image Slider won't work until you setup a View called 'imageslider'.  The view should query for the image nodes you want to display.", 'error');
    return;
  }
  $output = views_build_view('block', $view, array(), false);
  return $output;
}

function theme_views_view_imageslider($view, $type, $nodes) {
  // get imagecache preset
  $preset = imagecache_preset_by_name('imageslider');
  
  // extract the size of the image
  $width = $preset['actions'][0]['data']['width'];
  $height = $preset['actions'][0]['data']['height'];
  $images = array();
	$imagefield = variable_get('imageslider_image_field', ''); // TODO: this needs to be a configurable setting
  foreach ($nodes as $n) {
    $node = node_load($n->nid);
    $images[] = '<li>' . theme('imagecache', 'imageslider', $node->{$imagefield}[0]['filepath'], $node->title, $node->title, array('width' => $width, 'height' => $height)) . '</li>';
  }
  
  if ($images) {
    $overall_width = count($nodes) * $width;
    $output  = "<div class=\"imageslider\" style=\"width: {$width}px; height: {$height}px\">\n";
    $output .= "  <ul style=\"width: {$overall_width}px\">\n";
    $output .= implode("\n", $images);
    $output .= "  </ul>\n";
    $output .= "</div>\n";
  }
  
  return $output;
}

/**
 * Implementation of hook_settings
 */
function imageslider_admin_settings() {
  $form['imageslider_ease_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration of transition'),
    '#required' => TRUE,
    '#default_value' => variable_get('imageslider_ease_time', 750),
    '#description' => t('Time in milliseconds, defaults to 750'),
  );
  $path = drupal_get_path('module', 'imageslider');
  if (!file_exists($path . '/jquery.easing.js')) {
    drupal_set_message(t('jQuery easing plugin not found! The easing function will only be used if this plugin is downloaded from
      !url and placed in the imageslider module directory.', array('!url' => l('http://gsgd.co.uk/sandbox/jquery/easing/', 'http://gsgd.co.uk/sandbox/jquery/easing/'))));
  }
  
  // easing functions from the jQuery easing plugin
  $funcs = array(
    'jswing',
    'def',
    'easeInQuad',
    'easeOutQuad',
    'easeInOutQuad',
    'easeInCubic',
    'easeInCubic',
    'easeOutCubic',
    'easeInOutCubic',
    'easeInQuart',
    'easeOutQuart',
    'easeInOutQuart',
    'easeInQuint',
    'easeOutQuint',
    'easeInOutQuint',
    'easeInSine',
    'easeOutSine',
    'easeInOutSine',
    'easeInExpo',
    'easeOutExpo',
    'easeInOutExpo',
    'easeInCirc',
    'easeOutCirc',
    'easeInOutCirc',
    'easeInElastic',
    'easeOutElastic',
    'easeInOutElastic',
    'easeInBack',
    'easeOutBack',
    'easeInOutBack',
    'easeInBounce',
    'easeOutBounce',
    'easeInOutBounce'
  );
  $form['imageslider_ease_func'] = array(
    '#type' => 'select',
    '#title' => t('Easing function'),
    '#required' => TRUE,
    '#default_value' => variable_get('imageslider_ease_func', 'easeInOutExpo'),
    '#options' => array_combine($funcs, $funcs),
    '#description' => t('Function name to use from jQuery easing plugin, defaults to easeInOutExpo')
  );
  
  // find all imagefield fields
  $fields = content_fields();
  $choices = array();
  foreach ($fields as $name => $field) {
    if ($field['type'] == 'image') {
      $choices[$name] = $field['widget']['label'];
    }
  }
  if ($choices) {
    $form['imageslider_image_field'] = array(
      '#type' => 'select',
      '#title' => t('Image field to use'),
      '#default_value' => variable_get('imageslider_image_field', ''),
      '#options' => $choices,
      '#description' => t('Select the image field to use for rendering in the viewer.'),
    );
  } else {
    drupal_set_message("The node type selected doesn't have any image fields.  Please select a different node type, or add an image field to this node type.", 'error');
  }
  
  // check for existance of imagecache preset and warn user if it's missing
  $preset = imagecache_preset_by_name('imageslider');
  if (!$preset) {
     drupal_set_message("The Image Slider won't work until you setup an Imagecache preset named 'imageslider'.  The preset should resize and crop all images to a fixed width and height.", 'error');
  }
  
  // check for existance of Views configuration
  $view = views_get_view('imageslider');
  if (!$view) {
     drupal_set_message("The Image Slider won't work until you setup a View called 'imageslider'.  The view should query for the image nodes you want to display.", 'error');
  }
  
  return system_settings_form($form);
}
